//! Generic either-or type, similar to `Result` but more generic.
//!
//! # Usage
//!
//! ```
//! # use or_type::Or;
//! # struct Foo {}
//! # struct Bar {}
//! fn get_foo_or_bar(b: bool) -> Or<Foo, Bar> {
//!     if b {
//!         Or::Left(Foo {})
//!     } else {
//!         Or::Right(Bar {})
//!     }
//! }
//! ```

use std::fmt;
use std::hash::{Hash, Hasher};

pub enum Or<L, R> {
    Left(L),
    Right(R),
}

impl<L, R> Or<L, R> {
    /// Returns true if this holds the left variant
    pub fn is_left(&self) -> bool {
        matches!(self, Or::Left(_))
    }

    /// Returns true if this holds the right variant
    pub fn is_right(&self) -> bool {
        matches!(self, Or::Right(_))
    }
}

impl<L: fmt::Debug, R: fmt::Debug> fmt::Debug for Or<L, R> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Or::Left(l) => f.debug_tuple("Or").field(&l).finish(),
            Or::Right(r) => f.debug_tuple("Or").field(&r).finish(),
        }
    }
}

impl<L: Clone, R: Clone> Clone for Or<L, R> {
    fn clone(&self) -> Self {
        match self {
            Or::Left(l) => Or::Left(l.clone()),
            Or::Right(r) => Or::Right(r.clone()),
        }
    }
}

impl<L: Copy, R: Copy> Copy for Or<L, R> {}

impl<L: Hash, R: Hash> Hash for Or<L, R> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        "String to make 'k1 != Or<k1, T> where or holds k1'".hash(state);
        match self {
            Or::Left(l) => l.hash(state),
            Or::Right(r) => r.hash(state),
        }
    }
}
