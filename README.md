# Or

Generic either-or type, similar to Result but more generic

# Usage

```rust
fn get_foo_or_bar(b: bool) -> Or<Foo, Bar> {
    if b {
        Or::Left(Foo {})
    } else {
        Or::Right(Bar {})
    }
}
```
